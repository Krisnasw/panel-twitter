
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{!! (Auth::user()->avatar != null) ? Auth::user()->avatar : Avatar::create(Auth::user()->name)->toBase64() !!}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{!! Auth::user()->name !!}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
      @if(Auth::user()->role === 'member')
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="{{ url('/panel') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
      @else
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <a href="{{ url('/sbh-admin') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
          <a href="{{ url('/sbh-admin/tokens') }}">
            <i class="fa fa-book"></i> <span>Data Access Token</span>
          </a>
        </li>
      @endif
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>