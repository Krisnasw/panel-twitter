@extends('layouts.master')

@section('title')
	Panel SBH - Data Token
@endsection

@section('style')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
	<div class="content-wrapper">
	    <!-- Main content -->
	    <section class="content">

	      <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Token</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Access Token</th>
                  <th>Access Token Secret</th>
                  <th>Screen Name</th>
                  <th>User ID</th>
                </tr>
                </thead>
                <tbody>
                @php $no = 0; @endphp
                @foreach($data as $key)
                	<tr>
                		<td>{!! ++$no !!}</td>
                		<td>{!! $key->access_token !!}</td>
                		<td>{!! $key->access_token_secret !!}</td>
                		<td>{!! $key->users_id !!}</td>
                	</tr>
                @endforeach
                </tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

	    </section>
	    <!-- /.content -->
  	</div>
@endsection

@section('script')
	<!-- DataTables -->
	<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script>
	  $(function () {
	    $('#example2').DataTable({
	      'paging'      : true,
	      'lengthChange': false,
	      'searching'   : false,
	      'ordering'    : true,
	      'info'        : true,
	      'autoWidth'   : false
	    })
	  })
	</script>
@endsection