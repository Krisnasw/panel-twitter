<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Socialite;
use Validator;
use Alert;

class AuthController extends Controller
{
    //
    public function index()
    {
        if (Auth::check()) {
            # code...
            return redirect('/panel');
        } else {
            return view('welcome');
        }
    }

    public function doLogin(Request $request)
    {
        $valid = Validator::make($request->all(), [
                'username' => 'required|string|max:255',
                'password' => 'required|string'
            ]);

        if ($valid->passes()) {
            # code...
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'role' => 'admin'])) {
                # code...
                Alert::success("Selamat Datang !", "Success!");
                return redirect('/sbh-admin');
            } else {
                Alert::error("Username Atau Password Salah", "Error!");
                return redirect()->back()->withInput($request->all());
            }
        } else {
            Alert::info("Data Tidak Valid!", "Info!");
            return redirect()->back()->withInput($request->all());
        }
    }

    public function redirectToProvider()
    {
    	return Socialite::driver('twitter')->redirect();
    }

    public function handleProviderCallback()
    {
    	try {
    		$user = Socialite::driver('twitter')->user();
    	} catch (Exception $e) {
    		return redirect('auth/twitter');
    	}

    	$authUser = $this->findOrCreateUser($user);

    	Auth::login($authUser, true);

    	return redirect('panel');
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect('/');
    }

    private function findOrCreateUser($twitterUser)
    {
        $authUser = User::where('twitter_id', $twitterUser->id)->first();
 
        if ($authUser){
            return $authUser;
        }
 
        return User::create([
            'name' => $twitterUser->name,
            'handle' => $twitterUser->nickname,
            'twitter_id' => $twitterUser->id,
            'role' => 'member',
            'token' => $twitterUser->token,
            'token_secret' => $twitterUser->tokenSecret,
            'email' => $twitterUser->email,
            'avatar' => $twitterUser->avatar_original
        ]);
    }
}
