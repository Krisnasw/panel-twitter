<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $user = User::create([
        		'id' => 1,
        		'name' => 'Administrator',
        		'email' => 'admin@mailer.dev',
        		'username' => 'admin',
        		'password' => bcrypt('admin'),
        		'role' => 'admin'
        	]);
    }
}
